# Trabajo practico: Control de auto mediante Wireless
Control de un auto mediante wireless implementado con un Raspberry Pi 3
https://hackaday.io/project/1493-raspberry-pi-arduino-webcam-robot#menu-description

## Objetivos generales
En el siguiente proyecto, se pretende desarrollar un auto robótico que puede ser controlado mediante un servidor web, desde distintos dispositivos (Smartphone, tablet, notebook, etc.) que estén conectados a la misma red.
El auto contará con módulo Wifi para su comunicación, teniendo que acceder a una pagina web para poder controlarlo, además contará con un sensor de proximidad, el cual evitará que el mismo pueda colisionar mientras está
funcionando, una vez que el sensor detecte que el objeto que interpone el paso del mismo esté demasiado cerca, frenará los motores y esperará una respuesta del usuario para que pueda redirigir el sentido del auto.

## Materiales
- 1 - Raspberry pi 3 modelo B+
- 1 - Memoria Micro SD
- 1 - Sensor Ultrasonico Hc-sr05
- 1 - Adaptador Wireless USB
- 4 - Motor Caja Reductora Dc 3v A 6v Rueda Goma
- 4 - baterias AA
- Varios - Cables Dupont
- 1 - PowerBank
 
## Integrantes
- Cavanagh, Juan Francisco
- Lamberti, German
- Navarro, Matias Alejandro

## Licencia
GNU GPLv2
