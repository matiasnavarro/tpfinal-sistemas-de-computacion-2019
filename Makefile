obj-m+=helloWorld.o

all: module

module:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

up:
	sudo insmod helloWorld.ko

down:
	sudo rmmod helloWorld.ko
