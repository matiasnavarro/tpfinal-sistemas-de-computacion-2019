//import curses and GPIO
#include <curses.h>
#include <bcm2835.h>
#include <stdio.h>
#include <stdlib.h>

int main(){

    //set GPIO numbering mode and define output pins
    printf("Raspberry Pi wiringPi blink test\n");
  
    // if (wiringPiSetup() == -1) {
    //   printf( "Setup didn't work... Aborting." );
    //   exit (1);
    // }
    
    bcm2835_init();
    bcm2835_gpio_fsel(17, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(27, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(10, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(9, BCM2835_GPIO_FSEL_OUTP);
    // pinMode(11,OUTPUT);
    // pinMode(13,OUTPUT);
    // pinMode(19,OUTPUT);
    // pinMode(21,OUTPUT);

    //Get the curses window, turn off echoing of keyboard to screen, turn on
    //instant (no waiting) key response, and use special values for cursor keys
    initscr();
    noecho(); 
    cbreak();

    bcm2835_gpio_write(17, 0);
    bcm2835_gpio_write(27, 0);
    bcm2835_gpio_write(10, 0);
    bcm2835_gpio_write(9, 0);

    while(1){

        int tecla = getch();
        
        if (tecla == 'p'){
            break;
        }
        else if (tecla == 'a'){
            bcm2835_gpio_write(17, 0);
            bcm2835_gpio_write(27, 1);
            bcm2835_gpio_write(10, 0);
            bcm2835_gpio_write(9, 1);
	        printw("A \n");
        }
        else if (tecla == 'd'){
            bcm2835_gpio_write(17, 1);
            bcm2835_gpio_write(27, 0);
            bcm2835_gpio_write(10, 1);
            bcm2835_gpio_write(9, 0);
	    printw("D \n");

        }
	    else if (tecla == 's'){
            bcm2835_gpio_write(17, 1);
            bcm2835_gpio_write(27, 0);
            bcm2835_gpio_write(10, 0);
            bcm2835_gpio_write(9, 1);
	        printw("S \n");
        }
        else if (tecla == 'w'){
            bcm2835_gpio_write(17, 0);
            bcm2835_gpio_write(27, 1);
            bcm2835_gpio_write(10, 1);
            bcm2835_gpio_write(9, 0);
	        printw("W \n");
        }
        else if (tecla == 10){
            bcm2835_gpio_write(17, 0);
            bcm2835_gpio_write(27, 0);
            bcm2835_gpio_write(10, 0);
            bcm2835_gpio_write(9, 0);
	        printw("Enter \n");
        }
        else if (tecla == 'm'){
            printw("Alto \n");
        }
	    refresh();	    
        // print(distance('cm'))
    }

    //Close down curses properly, inc turn echo back on!
    nocbreak();  
    echo();
    endwin();

    return 0;
}
