//import curses and GPIO
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

int ultrason();

int main(){
    	
    //set GPIO numbering mode and define output pins
    printf("Raspberry Pi wiringPi blink test\n");
  
    if (wiringPiSetupGpio() == -1) {
      printf( "Setup didn't work... Aborting." );
      exit (1);
    }
    
    pinMode(12,INPUT);
    pinMode(16,OUTPUT);
    
    printf("Prueba 1 \n");
    while (1)
    {
	printf("In while");	
	int dist = 0;
        dist = ultrason();
        printf("distancia: %d \n", dist);
    }

    return 0;
}

int ultrason()
{
    int distance = 0;
    long startTime = 0;
    long travelTime = 0;

    digitalWrite(16, 0); //   TRIG pin must start off
    delay(30);

    //  Send trig pulse
    digitalWrite(16, 1); // On
    delayMicroseconds(20);
    digitalWrite(16, 0); // Off

    //  Wait for echo start
    while (digitalRead(12) == 0)
        ;

    //  Wait for echo end
    startTime = micros();

    while (digitalRead(12) == 1)
        ;

    travelTime = micros() - startTime;

    //Get distance in cm
    distance = travelTime / 58;

    return distance;
}

