//import curses and GPIO
#include <curses.h>
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include "cdecl.h"

int ultrason(void);
int main1(void);
int main2(void);
int llamadas(void);
void llamar(void);

int main(){

    //set GPIO numbering mode and define output pins
    printf("Raspberry Pi wiringPi blink test\n");
  
    if (wiringPiSetupGpio() == -1) {
      printf( "Setup didn't work... Aborting." );
      exit (1);
    }
    
    pinMode(12,INPUT); 	//Echo
    pinMode(16,OUTPUT);	//Triger

    pinMode(23,OUTPUT);
    pinMode(24,OUTPUT);	
    pinMode(17,OUTPUT);
    pinMode(27,OUTPUT);
    pinMode(10,OUTPUT);
    pinMode(9,OUTPUT);

    //Get the curses window, turn off echoing of keyboard to screen, turn on
    //instant (no waiting) key response, and use special values for cursor keys
    initscr();
    noecho();
    cbreak();

    while(1){
        int dist = 0;
        int tecla = getch();
        dist = ultrason();
        if(dist >  5){
           if(tecla == 'w'){
              digitalWrite(17, 0);
              digitalWrite(27, 1);
              digitalWrite(10, 1);
              digitalWrite(9, 0);	  	
	   }
        }

        if (tecla == 'p'){
            break;
        }
        else if(tecla == 'k'){
            llamar();	      	 	
        }
        else if (tecla == 'a'){
            digitalWrite(17, 0);
            digitalWrite(27, 1);
            digitalWrite(10, 0);
            digitalWrite(9, 1);
        }
        else if (tecla == 'd'){
            digitalWrite(17, 1);
            digitalWrite(27, 0);
            digitalWrite(10, 1);
            digitalWrite(9, 0);
        }
	else if (tecla == 's'){
            digitalWrite(17, 1);
            digitalWrite(27, 0);
            digitalWrite(10, 0);
            digitalWrite(9, 1);
        }
        else if (tecla == 10){
            digitalWrite(17, 0);
            digitalWrite(27, 0);
            digitalWrite(10, 0);
            digitalWrite(9, 0);
        }
        else if (tecla == 'm'){
            printw("Alto \n");
        }
	refresh();
	delay(30);
	digitalWrite(17, 0);
        digitalWrite(27, 0);
        digitalWrite(10, 0);
        digitalWrite(9, 0);
	    
        // print(distance('cm'))
    }

    digitalWrite(17, 0);
    digitalWrite(27, 0);
    digitalWrite(10, 0);
    digitalWrite(9, 0);

    //Close down curses properly, inc turn echo back on!
    nocbreak();  
    echo();
    endwin();

    return 0;
}

int ultrason()
{
    int distance = 0;
    long startTime = 0;
    long travelTime = 0;

    digitalWrite(16, 0); //   TRIG pin must start off
    delay(2);

    //  Send trig pulse
    digitalWrite(16, 1); // On
    delayMicroseconds(10);
    digitalWrite(16, 0); // Off

    //  Wait for echo start
    while (digitalRead(12) == 0)
        ;

    //  Wait for echo end
    startTime = micros();

    while (digitalRead(12) == 1)
        ;

    travelTime = micros() - startTime;

    //Get distance in cm
    distance = travelTime / 58;

    return distance;
}


int llamadas()
{
    static int flag=0;
    static int ret_status;    
    
    if(flag==0){
    
        ret_status = main1(); 
        flag=1;
    }
    else
    {
        ret_status = main2();
        flag=0;    
    }
    return ret_status;
}

void llamar(void){
    static int flag=0;
    static int ret_status;    
    
    if(flag==0){
	digitalWrite(23,1);
	digitalWrite(24,1);    
        flag=1;
    }
    else
    {
	digitalWrite(23,0);
	digitalWrite(24,0);
        flag=0;    
    }

}
