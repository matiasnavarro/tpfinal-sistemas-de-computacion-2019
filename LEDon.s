@ ---------------------------------------
@       Data Section
@ ---------------------------------------

         .data
         .balign 4
Intro:   .asciz  "Raspberry Pi wiringPi blink test\n"
ErrMsg:  .asciz "Setup didn't work... Aborting...\n"
pin:     .int   23
pin2:    .int   24
i:       .int   0
delayMs: .int   250
OUTPUT   =      1

@ ---------------------------------------
@       Code Section
@ ---------------------------------------

        .text
        .global main1
        .extern printf
        .extern wiringPiSetupGpio
        .extern delay
        .extern digitalWrite
        .extern pinMode

main1:   push    {ip, lr}        @ push return address + dummy register
                                @ for alignment

@  printf( "blink..." ) ;
        ldr     r0, =Intro
        bl      printf

@  if (wiringPiSetup() == -1) {
@     printf( "Setup didn't work... Aborting." ) ;
@     exit (1)                                   ;
@  }
        bl      wiringPiSetupGpio
        mov     r1,#-1
        cmp     r0, r1
        bne     init
        ldr     r0, =ErrMsg
        bl      printf
        b       done

@  pinMode(pin, OUTPUT)         ;
init:
        ldr     r0, =pin
        ldr     r0, [r0]
        mov     r1, #OUTPUT
        bl      pinMode

        ldr     r0, =pin2
        ldr     r0, [r0]
        mov     r1, #OUTPUT
        bl      pinMode

        ldr     r0, =pin
        ldr     r0, [r0]
        mov     r1, #1
        bl      digitalWrite

        ldr     r0, =pin2
        ldr     r0, [r0]
        mov     r1, #1
        bl      digitalWrite

done:
        pop     {ip, pc}        @ pop return address into pc
