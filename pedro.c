//import curses and GPIO
#include <curses.h>
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include "cdecl.h"

int main1(void);
int main2(void);
int llamadas();
int ultrason(void);

int main(){

    //set GPIO numbering mode and define output pins
    printf("Raspberry Pi wiringPi blink test\n");
  
    if (wiringPiSetupGpio() == -1) {
      printf( "Setup didn't work... Aborting." );
      exit (1);
    }
    
    pinMode(17,OUTPUT);
    pinMode(27,OUTPUT);
    pinMode(10,OUTPUT);
    pinMode(9,OUTPUT);
   
    pinMode(12,INPUT);  	//trigger
    pinMode(16,OUTPUT);		//echo
    //Get the curses window, turn off echoing of keyboard to screen, turn on
    //instant (no waiting) key response, and use special values for cursor keys
    initscr();
    noecho(); 
    cbreak();

    while(1){
        int dist = 0;
        int tecla = getch();
        dist = ultrason();
        if(dist >  5){
           if(tecla == 'w'){
              digitalWrite(17, 0);
              digitalWrite(27, 1);
              digitalWrite(10, 1);
              digitalWrite(9, 0);         
           }
        }

        if (tecla == 'p'){
            break;
        }
	else if(tecla == 'k'){
	    llamadas();
	}
        else if (tecla == 'a'){
            digitalWrite(17, 0);
            digitalWrite(27, 1);
            digitalWrite(10, 0);
            digitalWrite(9, 1);
	    printw("A \n");
        }
        else if (tecla == 'd'){
            digitalWrite(17, 1);
            digitalWrite(27, 0);
            digitalWrite(10, 1);
            digitalWrite(9, 0);
	    printw("D \n");
        }
	else if (tecla == 's'){
            digitalWrite(17, 1);
            digitalWrite(27, 0);
            digitalWrite(10, 0);
            digitalWrite(9, 1);
	    printw("S \n");
	}
	
        else if (tecla == 10){
            digitalWrite(17, 0);
            digitalWrite(27, 0);
            digitalWrite(10, 0);
            digitalWrite(9, 0);
	    printw("Enter \n");
        }
        else if (tecla == 'm'){
            printw("Alto \n");
        }
        refresh();
       // delay(20);
        digitalWrite(17, 0);
        digitalWrite(27, 0);
        digitalWrite(10, 0);
        digitalWrite(9, 0);
        // print(distance('cm'))
    }

    //Close down curses properly, inc turn echo back on!
    nocbreak();  
    echo();
    endwin();

    return 0;
}

int llamadas()
{
    static int flag=0;
    static int ret_status;    
    
    if(flag==0){
    
        ret_status = main1(); 
        flag=1;
        printf("hola");
    }
    else
    {
        ret_status = main2();
        flag=0;    
        printf("chau");
    }
    return ret_status;
}

int ultrason()
{
    int distance = 0;
    long startTime = 0;
    long travelTime = 0;

    digitalWrite(16, 0); //   TRIG pin must start off
    delay(30);

    //  Send trig pulse
    digitalWrite(16, 1); // On
    delayMicroseconds(20);
    digitalWrite(16, 0); // Off

    //  Wait for echo start
    while (digitalRead(12) == 0)
        ;

    //  Wait for echo end
    startTime = micros();

    while (digitalRead(12) == 1)
        ;

    travelTime = micros() - startTime;

    //Get distance in cm
    distance = travelTime / 58;

    return distance;
}
